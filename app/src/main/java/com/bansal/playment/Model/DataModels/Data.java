package com.bansal.playment.Model.DataModels;

/**
 * Created by BANSAL on 20/03/17.
 */

public class Data {


    private String name;
    private String year;
    private String type;

    public Data(String name,String year,String type, String desc)
    {
        this.name=name;
        this.year=year;
        this.type=type;
        this.desc=desc;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private String desc;


}
