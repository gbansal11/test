package com.bansal.playment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.TextView;

import com.bansal.playment.Model.DataModels.Data;
import com.bansal.playment.View.ImageListAdapter;
import com.bansal.playment.View.ItemTouchHelperCallback;
import com.bansal.playment.View.StartDragInterface;
import com.bansal.playment.View.ViewPresenterContract.HomeScreenContract;
import com.bansal.playment.presenter.HomePresenter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements HomeScreenContract.View,StartDragInterface {

    private HomeScreenContract.Presenter presenter;



    RecyclerView mRecyclerView;
    int i;
    private TextView name,year,type,desc;
    // Declaring RecyclerView
    private ItemTouchHelper mItemTouchHelper;

    ImageListAdapter mAdapter;                        // Declaring Adapter For Recycler View
    public ArrayList<Data> list = new ArrayList<Data>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.cardList); // Assigning the RecyclerView Object to the xml View
        mRecyclerView.setHasFixedSize(true);


        name = (TextView) findViewById(R.id.name);

        LinearLayoutManager mLayoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter =new ImageListAdapter(list,this);
        mRecyclerView.setAdapter(mAdapter);


        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(mAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);


        presenter=new HomePresenter(this);

        Data data= new Data("Titanic","1999","Movie","ASASASAS");
        Data data1= new Data("AVATAR","1999","Movie","ASASASAS");
        Data data2= new Data("GLADIAtOR","1999","Movie","ASASASAS");
        Data data3= new Data("ROCk","1999","Movie","ASASASAS");


        list.add(data);
        list.add(data1);
        list.add(data2);
        list.add(data3);
        list.add(data);list.add(data);


    }


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {


    }

    @Override
    public void onEndDrag(Data data) {

        name.setText(data.getName());

    }


}
