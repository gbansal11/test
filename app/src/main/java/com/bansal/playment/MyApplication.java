package com.bansal.playment;

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {
    private static Context context;


    @Override
    public void onCreate() {
        super.onCreate();
        try {
            context=getApplicationContext();

            //AppMainAPI.setupAPI();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static Context getAppContext(){
        return context;
    }
}
