package com.bansal.playment.presenter;

import com.bansal.playment.View.ViewPresenterContract.HomeScreenContract;

/**
 * Created by BANSAL on 20/03/17.
 */

public class HomePresenter implements HomeScreenContract.Presenter {

    private HomeScreenContract.View view;

    public HomePresenter(HomeScreenContract.View view)
    {
        this.view=view;
    }
}
