package com.bansal.playment.View;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bansal.playment.MainActivity;
import com.bansal.playment.Model.DataModels.Data;
import com.bansal.playment.R;
import com.bansal.playment.View.ViewPresenterContract.ItemTouchViewHolder;

import java.util.ArrayList;


public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> implements ItemTouchHelperAdapter {
    private  Activity mActivity;
    private  StartDragInterface mDragStartListener;

    private ArrayList<Data> mDataSet;

    @Override
    public void onItemDismiss(int position) {
        mDragStartListener.onEndDrag(mDataSet.get(position));

        mDataSet.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements ItemTouchViewHolder {
        public TextView type,name,year;
        public ImageView image;
        public RelativeLayout view;
        public ViewHolder(View v) {
            super(v);
            type= (TextView) v.findViewById(R.id.type);
            name= (TextView) v.findViewById(R.id.name);
            year= (TextView) v.findViewById(R.id.year);
            image= (ImageView) v.findViewById(R.id.image);
            view= (RelativeLayout) v.findViewById(R.id.card_item);


        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ImageListAdapter(ArrayList<Data> mDataSet,StartDragInterface mDrag) {
        this.mDataSet = mDataSet;
        mDragStartListener=mDrag;
        this.mActivity=(MainActivity)mDrag;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ImageListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_content, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.name.setText(mDataSet.get(position).getName());
        holder.type.setText(mDataSet.get(position).getType());
        holder.year.setText(mDataSet.get(position).getYear());
        holder.image.setBackground(mActivity.getResources().getDrawable(R.drawable.image1));

        holder.view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEventCompat.getActionMasked(event) ==
                        MotionEvent.ACTION_DOWN) {
                    mDragStartListener.onStartDrag(holder);
                }
                return false;
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

}



