package com.bansal.playment.View;

import android.support.v7.widget.RecyclerView;

import com.bansal.playment.Model.DataModels.Data;

/**
 * Created by BANSAL on 20/03/17.
 */

public interface StartDragInterface {
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
    void onEndDrag(Data data);

}
